package com.everis.apirest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

import com.everis.apirest.controller.resource.ClienteReducidoResource;
import com.everis.apirest.controller.resource.ClienteResource;
import com.everis.apirest.model.entity.Cliente;
import com.everis.apirest.service.ClienteService;

@RestController
public class ApiController {
	@Autowired
	ClienteService clienteService;
	
	@GetMapping("/clientes")
	public List<ClienteResource> obtenerClientes(){
		List<ClienteResource> listado = new ArrayList<>();
		clienteService.obtenerCliente().forEach(cliente->{
			ClienteResource clienteResource = new ClienteResource();
			clienteResource.setIdCliente(cliente.getIdCliente());
			clienteResource.setNombres(cliente.getNombres());
			clienteResource.setApellidos(cliente.getApellidos());
			clienteResource.setDireccion(cliente.getDireccion());
			listado.add(clienteResource);
		});
		return listado;
	}
	
	@GetMapping("cliente/{id}")
	public ClienteResource obtenerClientePorId(@PathVariable("id") Long id) throws Exception{
		ClienteResource clienteResource = new ClienteResource();
		Cliente cliente = clienteService.obtenerClientePorId(id);
		clienteResource.setIdCliente(cliente.getIdCliente());
		clienteResource.setApellidos(cliente.getApellidos());
		clienteResource.setNombres(cliente.getNombres());
		clienteResource.setDireccion(cliente.getDireccion());
		return clienteResource;
	}
	
	@PostMapping("/cliente")
	public ClienteResource crearCliente(@RequestBody ClienteReducidoResource request) throws Exception{
		Cliente intoCliente = new Cliente();
		ClienteResource clienteOut = new ClienteResource();
		intoCliente.setApellidos(request.getApellidos());
		intoCliente.setNombres(request.getNombres());
		intoCliente.setDireccion(request.getDireccion());
		Cliente cliente =  clienteService.crearCliente(intoCliente);
		clienteOut.setIdCliente(cliente.getIdCliente());
		clienteOut.setNombres(cliente.getNombres());
		clienteOut.setApellidos(cliente.getApellidos());
		clienteOut.setDireccion(cliente.getDireccion());
		return clienteOut;
	}

	
}
