package com.everis.apirest.controller.resource;

import lombok.Data;

@Data
public class ClienteReducidoResource {
	private String nombres;
	private String apellidos;
	private String direccion;
}
