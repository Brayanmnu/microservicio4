package com.everis.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Microservicio4Application {

	public static void main(String[] args) {
		SpringApplication.run(Microservicio4Application.class, args);
	}

}
