package com.everis.apirest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.apirest.model.entity.Cliente;
import com.everis.apirest.model.repository.ClienteRepository;

@Service
public class ClienteServiceImpl implements ClienteService{

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Override
	public Cliente crearCliente(Cliente request){
		return clienteRepository.save(request);
	}
	

	@Override
	public Cliente obtenerClientePorId(Long id) throws Exception {
		return clienteRepository.findById(id).orElseThrow(()->new Exception("Cliente no encontrado"));
	}

	
	@Override
	public Iterable<Cliente> obtenerCliente() {
		return clienteRepository.findAll();
	}
	

}
