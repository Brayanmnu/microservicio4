package com.everis.apirest.service;

import com.everis.apirest.model.entity.Cliente;
public interface ClienteService {
	public Cliente crearCliente(Cliente request);
	public Cliente obtenerClientePorId(Long id) throws Exception;
	public Iterable<Cliente> obtenerCliente();
}
